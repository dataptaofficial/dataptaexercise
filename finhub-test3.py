

import finnhub
import pandas as pd
#from pandas_datareader import data as pdr
import datetime as dt
from time import sleep

from collections import OrderedDict
from csv import DictReader

import ciso8601
import requests
import rx
from pytz import UTC
from rx import operators as ops

from influxdb_client import InfluxDBClient, WriteOptions
from influxdb_client.client.write.point import EPOCH

#########################################################################################################################################################
def main():
    ###################################################INITIALIZATIONS 
    #finnhub api key
    api_key= 'br9u23nrh5r8evkvbovg'

    print("history search")

    stock = 'BINANCE:BTCUSDT'
    resolution = '30'
    end_date = dt.datetime.now()
    start_date = end_date - dt.timedelta(days=15)
    end = int(end_date.timestamp())
    start = int(start_date.timestamp())

    finnhub_client = finnhub.Client(api_key=api_key)
    #result = finnhub_client.stock_candles(stock, resolution, start, end)
    result = finnhub_client.crypto_candles(stock, resolution, start, end)

    print(result)
    df = pd.DataFrame(result)
    dforig = pd.DataFrame(result)
    #df2=pd.to_datetime(df['t'], unit='s')
    
    df['t'] = pd.to_datetime(df['t'], unit='s')

    dforig2=df
    print("dforig2")
    print (dforig2)
    #print (df2)

    print("df")
    print(df)

    x = df.iloc[-1:]

    lasttime= x['t']
    #x.drop(x.index[x])

    ylast= lasttime.to_string(index=False, header=False)
    print (lasttime.to_string(index=False, header=False))
    print("lasttime "+str(ylast))

    print("x")
    print(x)

    #InfluxDB client and API Key
    client = InfluxDBClient(url="https://eu-central-1-1.aws.cloud2.influxdata.com", token="o95rsdG4GnYZEAb2D98Kbjzs6RH9S8U8BCbsg7tE8HWz9azsOUp_NRjQgKO55hAJYMqp9FUxFLtOYgOQvnZGBw==", org="be3972f46f58ed12", debug=False)
  
    #query example 
    '''from(bucket:"btcprices2")
            |> range(start: 0, stop: now())
            |> drop(columns: [])'''
    
    '''from(bucket: "btcprices2")
            |> range(start: v.timeRangeStart, stop: v.timeRangeStop)
            |> filter(fn: (r) => r["_field"] == "close")
            |> aggregateWindow(every: v.windowPeriod, fn: mean, createEmpty: false)
            |> yield(name: "mean")'''

    #now2 = pd.Timestamp().now('UTC')
    now2 = dt.datetime.now()
    print ("now2")
    print (now2)
    print (now2 + dt.timedelta(hours=1))

    data_frame = dforig2
    data_frame.rename(columns={'c': 'close', 'h': 'high', 'l': 'low', 'o': 'open', 's': 's', 't': '_time', 'v': 'volume'}, inplace=True)
    data_frame = data_frame [["_time", "close"]]
    data_frame.set_index("_time", inplace=True)
    print("data_frame")
    print(data_frame)

###################################################BACKFILLING
    #we actcually don't have tags but the api does not complain!
    write_api = client.write_api(write_options=WriteOptions(batch_size=50_000, flush_interval=10_000))
    write_api.write("btcprices3", record=data_frame, data_frame_measurement_name='BTC',
                    data_frame_tag_columns=['location'])
    #needed to see something in InfluxDB (flush)
    write_api.close() 
###################################################DELTA MODE
    #each X seconds we compare the time of the last reading with the time of the new last reading. If the times are different it means 30' have passed because the resolution we use is 30'
    while True:
        now = dt.datetime.now()
        print("now")
        print (now)
        minutes = str(now).split(":")[1]
        print (minutes)

        end_date = dt.datetime.now()
        start_date = end_date - dt.timedelta(days=1)
        end = int(end_date.timestamp())
        start = int(start_date.timestamp())

        finnhub_client = finnhub.Client(api_key=api_key)
        #result = finnhub_client.stock_candles(stock, resolution, start, end)
        result = finnhub_client.crypto_candles(stock, resolution, start, end)

        #print(result)
        df = pd.DataFrame(result)
        dforig2 = df

        #take the new last date 
        df['t'] = pd.to_datetime(df['t'], unit='s')
        
        x = df.iloc[-1:]

        lasttime= x['t'] 

        ynow= lasttime.to_string(index=False, header=False)

        print("---")
        print (ylast)
        print (ynow)
        print("---")

        if ylast!= ynow:    
            print ("change in time")
            ylast=ynow
            
            data_frame = dforig2
            data_frame.rename(columns={'c': 'close', 'h': 'high', 'l': 'low', 'o': 'open', 's': 's', 't': '_time', 'v': 'volume'}, inplace=True)
            data_frame = data_frame [["_time", "close"]]
            data_frame.set_index("_time", inplace=True)
            print("data_frame")
            print(data_frame)
            
            #measurement is like a table in sql
            #tags are like the indexes in sql
            write_api = client.write_api(write_options=WriteOptions(batch_size=50_000, flush_interval=10_000))
            write_api.write("btcprices3", record=data_frame, data_frame_measurement_name='BTC',data_frame_tag_columns=['location'])
            write_api.close()

        #change it for a reasonable refresh rate
        sleep(2)

#########################################################################################################################################################
if __name__ == '__main__':
    main()    